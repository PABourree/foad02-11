<?php

//Connexion BDD
require_once "db_connect.php";
require_once "function.php";
require_once "phppoo.php";

/* PHASE LOGIQUE = on traite les données puis on prépare les données pour l'affichage */

//$livres = recupere_bdd_plusieurs("SELECT * FROM livre");
$eleve = array();


$requete = "select eleveid, prenom, nom, datenais, moyenne, appreciation FROM eleve";
$stmt = $GLOBALS['lien_bdd']->prepare($requete);
$stmt->execute();
$stmt->bind_result($res_eleveid, $res_prenom, $res_nom, $res_datenais, $res_moy, $res_appreciation);
while ($stmt->fetch()) {
	$eleve[] = Eleve::construit_params($res_eleveid, $res_prenom, $res_nom, $res_datenais, $res_moy, $res_appreciation);
}


/* PHASE RENDU */
?>

<!DOCTYPE html>
<html>
<head>
<title>Eleve fiche</title>
</head>
<body>
<a href="fiche.php">Ajouter un éleve</a>
<table>
	<thead>
		<tr>
			<th>Nom</th>
			<th>Prenom</th>
			<th>Date de naissance</th>
            <th>moyenne</th>
            <th>appreciation</th>
		</tr>
	</thead>
	<tbody>
		
<?php for ($i=0; $i<count($eleve); $i++) { ?>
		<tr>
            <td><?= $eleve[$i]->nom ?></td>
			<td><?= $eleve[$i]->prenom ?></td>
			<td><?= $eleve[$i]->date_nais?></td>
			<td><?= $eleve[$i]->moy ?></td>
            <td><?= $eleve[$i]->appreciation ?></td>
		</tr>
<?php } ?>
		
	</tbody>
	<tfoot>
		<tr>
            <th>Nom</th>
			<th>Prenom</th>
			<th>Date de naissance</th>
            <th>moyenne</th>
            <th>appreciation</th>
		</tr>
	</tfoot>
</table>
</body>
</html>

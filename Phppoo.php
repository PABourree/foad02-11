<?php

class Eleve {

    public $eleveid;
    public $nom;
	public $prenom;
	public $date_nais;
	public $moy;
    public $appreciation;
    
    function __construct() {
        $this->eleveid = 0;
        $this->nom = "";
        $this->prenom = "";
        $this->date_nais = "";
        $this->moy = 0;
        $this->appreciation = "";
    }
    public static function construit_params($eleveid, $nom, $prenom, $date_nais, $moy, $appreciation) {
        $eleve = new Eleve();

        $eleve->eleveid = $eleveid;
        $eleve->nom = $nom;
        $eleve->prenom = $prenom;
        $eleve->date_nais = $date_nais;
        $eleve->moy = $moy;
        $eleve->appreciation = $appreciation;

        return $eleve;
}
public static function loadUnEleve($eleveid) {
    $eleve = new Eleve();
    
    //sql
    $requete = "select nom, prenom, datenais, moyenne, appreciation from eleve where eleveid = 0";
    $stmt = $GLOBALS['lien_bdd']->prepare($requete);
    $stmt->bind_param("i", $eleveid);
    $stmt->execute();
    $stmt->bind_result($nom, $prenom, $date_nais, $moy ,$appreciation);
    $stmt->fetch();
    $eleve->eleveid = $eleveid;
    $eleve->nom = $nom;
    $eleve->prenom = $prenom;
    $eleve->date_nais = $date_nais;
    $eleve->moy = $moy;
    $eleve->appreciation = $appreciation;
    
    return $eleve;
}
public function getDateFormatSql() {
    return $this->date_edition->format("Y-m-d");
}

public function setNom($nom) {
        $this->nom = assainit_texte($nom);
}

public function setDatenais($date_nais) {
        $this->date_nais = new DateTime(assainit_texte($date_nais));
}

public function setPrenom($prenom) {
        $this->prenom = assainit_texte($prenom);
}

public function setMoy($moy) {
        $this->moy = assainit_entier($moy);
}

public function setEleveId($eleveid) {
        $this->eleveid = assainit_entier($eleveid);
}
public function setAppreciation($appreciation) {
    $this->appreciation = assainit_texte($appreciation);
}

}

?>
<?php

require_once "db_connect.php";
require_once "function.php";
require_once "Phppoo.php";

if (!empty($_POST)) {
    echo "1";
    $eleve = new Eleve();

    $eleve->setNom("nom");
    $eleve->setPrenom("prenom");
    $eleve->setDatenais("datenais");
    $eleve->setMoy("moy");
    $eleve->setAppreciation("appreciation");
    $eleve->setEleveId("eleveid");


    if ($id){
        echo "3";
        $requete = "UPDATE eleve SET nom = ?, prenom = ?, datenais= ?, moyenne = ?, appreciation = ? WHERE `eleve`.`eleveid` = ?;";
        $stmt = $GLOBALS['lien_bdd']->prepare($requete);

        $stmt->bind_param("sssisi", $r_nom, $r_prenom, $r_datenais, $r_moy, $r_appreciation, $r_eleveid);
        $r_nom = $nom->nom;
        $r_prenom = $prenom->prenom;
        $r_datenais = $datenais->datenais;
        $r_moy = $moy->moy;
        $r_appreciation = $appreciation->appreciation;
        $r_eleveid = $eleveid->eleveid;
        

        $stmt->execute();
		$stmt->close();
    } else {
        echo "4";
        $requete = "INSERT INTO eleve (nom, prenom, datenais, moyenne, appreciation) VALUES (?, ?, ?, ?, ?); ";
        $stmt = $GLOBALS['lien_bdd']->prepare($requete);
        
        $stmt->bind_param("sssis", $nom, $prenom, $datenais, $moy, $appreciation);
        $r_nom = $nom->nom;
        $r_prenom = $prenom->prenom;
        $r_datenais = $datenais->datenais;
        $r_moy = $moy->moy;
        $r_appreciation = $appreciation->appreciation;


        $stmt->execute();
		$eleve->id = $stmt->insert_id;
        $stmt->close();
    }
    header("Location: fiche.php?id=$id");
} else {
    echo "6";
}
if (!empty($_GET['eleveid'])) {
    echo "2";
	
	$id = intval($_GET['eleveid']);
	
	$eleve = Eleve::loadUnEleve($eleveid);
} else {
    echo "5";
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Eleve fiche</title>
</head>
<body>
<form method="POST">
<input type="text" name="nom" placeholder="Nom éleve" value="<?php if (isset($eleve)) { echo $nom->nom; } ?>"/>
<input type="text" name="prenom" placeholder="Prénom éleve" value="<?php if (isset($eleve)) { echo $prenom->prenom; } ?>"/>
<input type="text" name="datenais" placeholder="Date de naissance de l'éleve" value="<?php if (isset($eleve)) { echo $eleve->getDateFormatSql(); } ?>"/>
<input type="text" name="moy" placeholder="Moyenne de l'éleve" value="<?php if (isset($eleve)) { echo $moy->moy; } ?>"/>
<input type="text" name="appreciation" placeholder="Appréciation de l'éleve" value="<?php if (isset($eleve)) { echo $appreciation->appreciation; } ?>"/>
<input type="hidden" name="eleveid" value="<?php if (isset($eleve)) { echo $eleve->eleveid; } ?>"/>
<input type="submit" />
</form>
</body>
</html>